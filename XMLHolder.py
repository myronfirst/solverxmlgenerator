import os


def FNameXML(outFilename, templateFilename):
    name = ''
    with open(templateFilename, 'rt') as f:
        name = os.path.abspath(f.name)
    name = name.replace(templateFilename, outFilename)
    return f'''\
    <fname>
        <n>{len(name)}</n>
        <s>{name}</s>
    </fname>'''


def LoadCaseXML():
    return '''\
    <arrLoadCases>
	    <GUID>680304d2-3acb-11e0-a9b1-001cc0947d6</GUID>
	    <catg>LC_PERMANENT_1</catg>
	    <gravity>1</gravity>
	    <self_weight_coeff>0.0</self_weight_coeff>
        <combo_coeff>0.0</combo_coeff>
	<name>
	    <name>1</name>
	</name>
	</arrLoadCases>'''


def MaterialXML():
    return '''\
    <arrMaterials>
        <typmat>eRAF_MAT_TYPE_ISO_1</typmat>
        <M>0</M>
        <SW>25</SW>
        <T>0</T>
        <E>29000000</E>
        <G>12083333.3333333</G>
        <V>0.2</V>
        <TH>1e-05</TH>
        <EY>0.002</EY>
        <EU>0.002</EU>
        <FK>20000</FK>
        <FD>13333.3333333333</FD>
        <FVK0>0</FVK0>
        <name>
            <name>1</name>
        </name>
    </arrMaterials>'''


def SectionXML():
    return '''\
    <arrSections>
        <mat>1</mat>
        <ntype>SEC_TYPE_PRISM_1</ntype>
        <A>0.16175</A>
        <S22>0.0625</S22>
        <S33>0.0625</S33>
        <J>0.000188384506036486</J>
        <I33>0.00177141619171819</I33>
        <I22>0.000985910677083333</I22>
        <A_SW>0.10625</A_SW>
        <WIDTH>0</WIDTH>
        <HEIGHT>0</HEIGHT>
        <NRD>0</NRD>
        <name>
            <name>1</name>
        </name>
    </arrSections>'''


def ShellSectionXML():
    return '''\
    <arrShellSections>
        <mat>1</mat>
        <ntype>SHELL_1</ntype>
        <th>0.20</th>
        <matang>0</matang>
        <name>
            <name>1</name>
        </name>
    </arrShellSections>'''


def StoreyXML():
    return '''\
    <arrStoreys>
        <GUID>680304dc-3acb-11e0-a9b1-001cc0947d6</GUID>
        <psi2>#y</psi2>
        <name>
          <name>1</name>
        </name>
  </arrStoreys>'''


def NodeXML():
    return '''\
    <arrNodes>
        <GUID>a96ed621-3e97-11ea-a066-fcaa14e60d6</GUID>
        <xyz>
            <x>#x</x>
            <y>#y</y>
            <z>#z</z>
        </xyz>
        <ang>
            <x>0</x>
            <y>0</y>
            <z>0</z>
        </ang>
        <name>
            <name>#name</name>
        </name>
    </arrNodes>
    '''


def ShellXML():
    return '''\
    <arrShells>
        <GUID>08da38fe-48ee-11ea-a068-fcaa14e60d6</GUID>
        <nodes>
            <n1>#node1</n1>
            <n2>#node2</n2>
            <n3>#node3</n3>
            <n4>#node4</n4>
        </nodes>
        <section>1</section>
        <rad_angle>0</rad_angle>
        <name>
            <name>#name</name>
        </name>
    </arrShells>
    '''


def RestraintXML():
    return '''\
    <arrRestraints>
        <node>#node</node>
        <f>
            <U1>1</U1>
            <U2>1</U2>
            <U3>1</U3>
            <R1>1</R1>
            <R2>1</R2>
            <R3>1</R3>
        </f>
    </arrRestraints>'''


def NodeMassXML():
    return '''\
    <arrNodeMasses>
        <m>
            <mx>1554.61009174313</mx>
            <my>1554.61009174313</my>
            <mz>0</mz>
            <Jmx>0</Jmx>
            <Jmy>0</Jmy>
            <Jmz>0</Jmz>
        </m>
        <node>#node</node>
    </arrNodeMasses>'''


def NodeForceXML():
    return '''\
    <arrNodeForces>
        <node>#node</node>
        <lc>1</lc>
        <f>
            <Fx>0</Fx>
            <Fy>100</Fy>
            <Fz>0</Fz>
            <Mx>0</Mx>
            <My>0</My>
            <Mz>0</Mz>
        </f>
    </arrNodeForces>'''


def BeamXML():
    return '''\
    <arrBeams>
        <GUID>ba12be63-3ca4-11e4-af0a-001cc0947d6</GUID>
        <angle>0</angle>
        <ecc3>0</ecc3>
        <soilks>
            <x>0</x>
            <y>0</y>
            <z>0</z>
        </soilks>
        <area_reduction_factor>1</area_reduction_factor>
        <endsI>
            <node>#node_start</node>
            <rs>0</rs>
            <theta_curve_id>0</theta_curve_id>
            <rel>
                <U1>0</U1>
                <U2>0</U2>
                <U3>0</U3>
                <R1>0</R1>
                <R2>0</R2>
                <R3>0</R3>
            </rel>
            <offsets_glob>
                <RigX>0</RigX>
                <RigY>0</RigY>
                <RigZ>0</RigZ>
                <RigZ2>0</RigZ2>
            </offsets_glob>
            <offsets_locl>
                <RigX>0</RigX>
                <RigY>0</RigY>
                <RigZ>0</RigZ>
                <RigZ2>0</RigZ2>
            </offsets_locl>
            <kappa>
                <x>0</x>
                <y>0</y>
                <z>0</z>
            </kappa>
            <lp>
                <x>0.0</x>
                <y>0.0</y>
            </lp>
            <Lv>
                <x>0</x>
                <y>0</y>
            </Lv>
            <NEd_Gpsi2Q>0</NEd_Gpsi2Q>
        </endsI>
        <endsJ>
            <node>#node_end</node>
            <rs>0</rs>
            <theta_curve_id>0</theta_curve_id>
            <rel>
                <U1>0</U1>
                <U2>0</U2>
                <U3>0</U3>
                <R1>0</R1>
                <R2>0</R2>
                <R3>0</R3>
            </rel>
            <offsets_glob>
                <RigX>0</RigX>
                <RigY>0</RigY>
                <RigZ>0</RigZ>
                <RigZ2>0</RigZ2>
            </offsets_glob>
            <offsets_locl>
                <RigX>0</RigX>
                <RigY>0</RigY>
                <RigZ>0</RigZ>
                <RigZ2>0</RigZ2>
            </offsets_locl>
            <kappa>
                <x>0</x>
                <y>0</y>
                <z>0</z>
            </kappa>
            <lp>
                <x>0.0</x>
                <y>0.0</y>
            </lp>
            <Lv>
                <x>0</x>
                <y>0</y>
            </Lv>
            <NEd_Gpsi2Q>0</NEd_Gpsi2Q>
        </endsJ>
        <section>1</section>
        <section_J>1</section_J>
        <varilocaxis>0</varilocaxis>
        <nsegments>15</nsegments>
        <nsub2>0</nsub2>
        <beam_type>BEAM_0</beam_type>
        <beam_kind>RCNORMAL_1</beam_kind>
        <do_push>0</do_push>
        <name>
            <name>#name</name>
        </name>
    </arrBeams>
    '''


def BeamDistrLoadXML():
    return '''\
    <arrBeamDistrLoads>
        <trap>
            <q1>
            <x>0</x>
            <v>-5</v>
            </q1>
            <q2>
            <x>1</x>
            <v>-5</v>
            </q2>
        </trap>
        <beam>#beam</beam>
        <lc>1</lc>
        <dir>UZ_3</dir>
    </arrBeamDistrLoads>'''


def BeamConcLoadXML():
    return '''\
    <arrBeamConcLoads>
        <dist>1.0</dist>
        <load>1.0</load>
        <beam>#beam</beam>
        <lc>1</lc>
        <dir>UZ_3</dir>
    </arrBeamConcLoads>'''


class XMLHolder:
    def __init__(self, outFilename, templateFilename):
        self.nodeNameCounter = 1
        self.beamNameCounter = 1
        self.shellNameCounter = 1
        self.map = {}

        self.Install('fname', FNameXML(outFilename, templateFilename))
        self.Install('material', MaterialXML())
        self.Install('load_case', LoadCaseXML())
        self.Install('section', SectionXML())
        self.Install('shell_section', ShellSectionXML())
        self.Install('storey', StoreyXML())
        self.Install('node', NodeXML())
        self.Install('beam', BeamXML())
        self.Install('shell', ShellXML())
        self.Install('restraint', RestraintXML())
        self.Install('node_mass', NodeMassXML())
        self.Install('node_force', NodeForceXML())
        self.Install('beam_distr_load', BeamDistrLoadXML())
        self.Install('beam_conc_load', BeamConcLoadXML())

    def Install(self, key, val):
        self.map[key] = val.strip()

    def GetFName(self):
        return self.map['fname'].strip()

    def GetLoadCase(self):
        return self.map['load_case'].strip()

    def GetMaterial(self):
        return self.map['material'].strip()

    def GetSection(self):
        return self.map['section'].strip()

    def GetShellSection(self):
        return self.map['shell_section'].strip()

    def GetStorey(self, y):
        return self.map['storey'].replace('#y', str(y)).strip()

    def GetNode(self, x, y, z):
        ret = self.map['node'].replace('#x', str(x)).replace('#y', str(y)).replace(
            '#z', str(z)).replace('#name', str(self.nodeNameCounter)).strip()
        self.nodeNameCounter += 1
        return ret

    def GetBeam(self, nodeStart, nodeEnd):
        ret = self.map['beam'].replace('#node_start', str(nodeStart)).replace(
            '#node_end', str(nodeEnd)).replace("#name", str(self.beamNameCounter)).strip()
        self.beamNameCounter += 1
        return ret

    def GetShell(self, node1, node2, node3, node4):
        ret = self.map['shell'].replace('#node1', str(node1)).replace('#node2', str(node2)).replace(
            '#node3', str(node3)).replace('#node4', str(node4)).replace("#name", str(self.shellNameCounter)).strip()
        self.shellNameCounter += 1
        return ret

    def GetRestraint(self, node):
        return self.map['restraint'].replace('#node', str(node)).strip()

    def GetNodeMass(self, node):
        return self.map['node_mass'].replace('#node', str(node)).strip()

    def GetNodeForce(self, node):
        return self.map['node_force'].replace('#node', str(node)).strip()

    def GetBeamDistrLoad(self, beam):
        return self.map['beam_distr_load'].replace('#beam', str(beam)).strip()

    def GetBeamConcLoad(self, beam):
        return self.map['beam_conc_load'].replace('#beam', str(beam)).strip()
