import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *

from utils import ToPythonIndex


class Vec3:
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

    def __eq__(self, other):
        return (self.x == other.x) and (
            self.y == other.y) and (self.z == other.z)

    def __neg__(self):
        return Vec3(-self.x, -self.y, -self.z)

    def __add__(self, other):
        return Vec3(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        return self + (-other)


class XMLRenderer:
    def __init__(self, xmlTree, initialDepth):
        self.xmlTree = xmlTree
        self.initialDepth = initialDepth
        self.rotAnchor = Vec3(0.0, 0.0, 0.0)
        self.transAnchor = Vec3(0.0, 0.0, 0.0)

    def RenderLoadCases(self, loadCases):
        pass

    def RenderMaterials(self, materials):
        pass

    def RenderSections(self, sections):
        pass

    def RenderShellSections(self, shellSections):
        pass

    def RenderStoreys(self, storeys):
        hr = 0.2
        glBegin(GL_LINES)
        glColor3f(1.0, 1.0, 1.0)
        for s in storeys:
            y = s.psi2
            glVertex3f(-1.0 - hr, y, 0)
            glVertex3f(-1.0 + hr, y, 0)
            glVertex3f(-1.0, y - hr, 0)
            glVertex3f(-1.0, y + hr, 0)
            glVertex3f(-1.0, y, 0 - hr)
            glVertex3f(-1.0, y, 0 + hr)
        glEnd()

    def RenderNodes(self, nodes):
        glBegin(GL_POINTS)
        glColor3f(1.0, 0.0, 0.0)
        for n in nodes:
            p = n.xyz
            glVertex3f(p.x, p.y, p.z)
        glEnd()

    def RenderShells(self, shells):
        glBegin(GL_QUADS)
        glColor4f(0.0, 0.0, 1.0, 0.2)
        for s in shells:
            n1, n2, n3, n4 = ToPythonIndex(
                s.nodes.n1), ToPythonIndex(
                s.nodes.n2), ToPythonIndex(
                s.nodes.n3), ToPythonIndex(
                s.nodes.n4)
            p1, p2, p3, p4 = self.xmlTree.arrNodes[n1].xyz, self.xmlTree.arrNodes[
                n2].xyz, self.xmlTree.arrNodes[n3].xyz, self.xmlTree.arrNodes[n4].xyz
            glVertex3f(p1.x, p1.y, p1.z)
            glVertex3f(p2.x, p2.y, p2.z)
            glVertex3f(p3.x, p3.y, p3.z)
            glVertex3f(p4.x, p4.y, p4.z)
        glEnd()

    def RenderRestraints(self, restraints):
        hr = 0.2
        glBegin(GL_QUADS)
        glColor3f(1.0, 1.0, 0.0)
        for r in restraints:
            i = ToPythonIndex(r.node)
            p = self.xmlTree.arrNodes[i].xyz
            glVertex3f(p.x - hr, p.y, p.z - hr)
            glVertex3f(p.x - hr, p.y, p.z + hr)
            glVertex3f(p.x + hr, p.y, p.z + hr)
            glVertex3f(p.x + hr, p.y, p.z - hr)
        glEnd()

    def RenderNodeMasses(self, nodeMasses):
        pass

    def RenderNodeForces(self, nodeForces):
        pass

    def RenderBeams(self, beams):
        glBegin(GL_LINES)
        glColor3f(0.0, 1.0, 0.0)
        for b in beams:
            i, j = ToPythonIndex(b.endsI.node), ToPythonIndex(b.endsJ.node)
            fromPos, toPos = self.xmlTree.arrNodes[i].xyz, self.xmlTree.arrNodes[j].xyz
            glVertex3f(fromPos.x, fromPos.y, fromPos.z)
            glVertex3f(toPos.x, toPos.y, toPos.z)
        glEnd()

    def RenderBeamDistrLoads(self, beamDistrLoads):
        pass

    def RenderBeamConcLoads(self, beamConcLoads):
        pass

    def Render(self):
        self.RenderShells(self.xmlTree.arrShells)
        self.RenderStoreys(self.xmlTree.arrStoreys)
        self.RenderLoadCases(self.xmlTree.arrLoadCases)
        self.RenderMaterials(self.xmlTree.arrMaterials)
        self.RenderSections(self.xmlTree.arrSections)
        self.RenderShellSections(self.xmlTree.arrShellSections)
        self.RenderNodes(self.xmlTree.arrNodes)
        self.RenderRestraints(self.xmlTree.arrRestraints)
        self.RenderNodeMasses(self.xmlTree.arrNodeMasses)
        self.RenderNodeForces(self.xmlTree.arrNodeForces)
        self.RenderBeams(self.xmlTree.arrBeams)
        self.RenderBeamDistrLoads(self.xmlTree.arrBeamDistrLoads)
        self.RenderBeamConcLoads(self.xmlTree.arrBeamConcLoads)

    def Run(self):
        pygame.init()
        display = (1024, 768)
        pygame.display.set_mode(display, DOUBLEBUF | OPENGL)
        glEnable(GL_PROGRAM_POINT_SIZE)
        glPointSize(10.0)
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        gluPerspective(45, (display[0] / display[1]), 0.1, 100.0)

        FPS = 60
        clock = pygame.time.Clock()
        isFinished = False
        glTranslatef(0.0, 0.0, self.initialDepth)

        while not isFinished:
            rotVec = Vec3(0, 0, 0)
            transVec = Vec3(0, 0, 0)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    isFinished = True
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        isFinished = True
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if pygame.mouse.get_pressed()[0]:
                        pos = Vec3(
                            pygame.mouse.get_pos()[0],
                            pygame.mouse.get_pos()[1],
                            0)
                        self.rotAnchor = pos
                    elif pygame.mouse.get_pressed()[2]:
                        pos = Vec3(
                            pygame.mouse.get_pos()[0],
                            pygame.mouse.get_pos()[1],
                            0)
                        self.transAnchor = pos
                elif event.type == pygame.MOUSEMOTION:
                    if pygame.mouse.get_pressed()[0]:
                        pos = Vec3(
                            pygame.mouse.get_pos()[0],
                            pygame.mouse.get_pos()[1],
                            0)
                        rotVec = pos - self.rotAnchor
                        self.rotAnchor = pos
                    elif pygame.mouse.get_pressed()[2]:
                        pos = Vec3(
                            pygame.mouse.get_pos()[0],
                            pygame.mouse.get_pos()[1],
                            0)
                        transVec = pos - self.transAnchor
                        self.transAnchor = pos

            glRotatef(rotVec.y, 1, 0, 0)
            glRotatef(rotVec.x, 0, 1, 0)
            glTranslatef(0.0, 0.0, -transVec.y)
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            self.Render()
            pygame.display.flip()
            clock.tick(FPS)
