def ToPythonIndex(i):
    if i is None:
        return None
    return i - 1


def ToSolverIndex(i):
    if i is None:
        return None
    return i + 1
