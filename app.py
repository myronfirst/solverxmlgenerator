import argparse

import solver_schema_api as SolverSchemaApi
from utils import ToSolverIndex
from XMLRenderer import XMLRenderer
from XMLHolder import XMLHolder


class App:
    def __init__(self, sizeX, sizeY, sizeZ, stepX, stepY,
                 stepZ, outFilename, templateFilename):
        self.sizeX = sizeX
        self.sizeY = sizeY
        self.sizeZ = sizeZ
        self.stepX = stepX
        self.stepY = stepY
        self.stepZ = stepZ
        self.outFilename = outFilename
        self.templateFilename = templateFilename
        self.xmlNodes = []
        self.xmlStoreys = []
        self.xmlBeams = []
        self.xmlShells = []
        self.xmlRestraints = []
        self.xmlNodeMasses = []
        self.xmlNodeForces = []
        self.xmlBeamDistrLoads = []
        self.xmlBeamConcLoads = []
        self.xmlSections = []
        self.xmlShellSections = []
        self.xmlHolder = XMLHolder(outFilename, templateFilename)

    def GetNodeIndex(self, x, y, z):
        assert x >= 0 and y >= 0 and z >= 0
        index = x + self.sizeX * (y + self.sizeY * z)
        return index

    def GetLineX(self, y, z):
        l = []
        for x in range(1, self.sizeX, 1):
            prevIndex = ToSolverIndex(self.GetNodeIndex(x - 1, y, z))
            index = ToSolverIndex(self.GetNodeIndex(x, y, z))
            l.append((prevIndex, index))
        return l

    def GetLineY(self, x, z):
        l = []
        for y in range(1, self.sizeY, 1):
            prevIndex = ToSolverIndex(self.GetNodeIndex(x, y - 1, z))
            index = ToSolverIndex(self.GetNodeIndex(x, y, z))
            l.append((prevIndex, index))
        return l

    def GetLineZ(self, x, y):
        l = []
        for z in range(1, self.sizeZ, 1):
            prevIndex = ToSolverIndex(self.GetNodeIndex(x, y, z - 1))
            index = ToSolverIndex(self.GetNodeIndex(x, y, z))
            l.append((prevIndex, index))
        return l

    def GetShellX(self, y, z):
        l = []
        for x in range(1, self.sizeX, 1):
            n1 = ToSolverIndex(self.GetNodeIndex(x - 1, y - 1, z))
            n2 = ToSolverIndex(self.GetNodeIndex(x - 1, y, z))
            n3 = ToSolverIndex(self.GetNodeIndex(x, y, z))
            n4 = ToSolverIndex(self.GetNodeIndex(x, y - 1, z))
            l.append((n1, n2, n3, n4))
        return l

    def GetShellY(self, z, x):
        l = []
        for y in range(1, self.sizeY, 1):
            n1 = ToSolverIndex(self.GetNodeIndex(x, y - 1, z - 1))
            n2 = ToSolverIndex(self.GetNodeIndex(x, y - 1, z))
            n3 = ToSolverIndex(self.GetNodeIndex(x, y, z))
            n4 = ToSolverIndex(self.GetNodeIndex(x, y, z - 1))
            l.append((n1, n2, n3, n4))
        return l

    def GetShellZ(self, x, y):
        l = []
        for z in range(1, self.sizeZ, 1):
            n1 = ToSolverIndex(self.GetNodeIndex(x - 1, y, z - 1))
            n2 = ToSolverIndex(self.GetNodeIndex(x - 1, y, z))
            n3 = ToSolverIndex(self.GetNodeIndex(x, y, z))
            n4 = ToSolverIndex(self.GetNodeIndex(x, y, z - 1))
            l.append((n1, n2, n3, n4))
        return l

    def GenerateStoreys(self):
        for y in range(0, self.sizeY, self.stepY):
            n = self.xmlHolder.GetStorey(y)
            self.xmlStoreys.append(n)

    def GenerateNodes(self):
        for _ in range(self.sizeX * self.sizeY * self.sizeZ):
            self.xmlNodes.append(None)
        for x in range(0, self.sizeX, self.stepX):
            for y in range(0, self.sizeY, self.stepY):
                for z in range(0, self.sizeZ, self.stepZ):
                    n = self.xmlHolder.GetNode(x, y, z)
                    i = self.GetNodeIndex(x, y, z)
                    self.xmlNodes[i] = n
        assert self.xmlNodes.count(None) == 0

    def GenerateBeams(self):
        for GetLine, size1, size2 in [(self.GetLineX, self.sizeY, self.sizeZ), (
                self.GetLineY, self.sizeX, self.sizeZ), (self.GetLineZ, self.sizeX, self.sizeY)]:
            for axis1 in range(size1):
                for axis2 in range(size2):
                    for nodeStart, nodeEnd in GetLine(axis1, axis2):
                        n = self.xmlHolder.GetBeam(nodeStart, nodeEnd)
                        self.xmlBeams.append(n)

    def GenerateShells(self):
        for GetShell, size1, size2 in [(
            self.GetShellX, self.sizeY, self.sizeZ), (
                self.GetShellY, self.sizeZ, self.sizeX), (
                    self.GetShellZ, self.sizeX, self.sizeY)]:
            for axis1 in range(1, size1):
                for axis2 in range(size2):
                    for n1, n2, n3, n4 in GetShell(axis1, axis2):
                        n = self.xmlHolder.GetShell(n1, n2, n3, n4)
                        self.xmlShells.append(n)

    def GenerateRestraints(self):
        for x in range(self.sizeX):
            for z in range(self.sizeZ):
                i = self.GetNodeIndex(x, 0, z)
                index = ToSolverIndex(i)
                n = self.xmlHolder.GetRestraint(index)
                self.xmlRestraints.append(n)

    # def GenerateMinimumRestraints(self):
    #     for x in range(self.sizeX):
    #         i = self.GetNodeIndex(x, 0, 0)
    #         index = ToSolverIndex(i)
    #         n = self.xmlHolder.GetRestraint(index)
    #         self.xmlRestraints.append(n)

    def GenerateNodeMasses(self):  # unused
        for i in range(0, len(self.xmlNodes), 1):
            index = ToSolverIndex(i)
            n = self.xmlHolder.GetNodeMass(index)
            self.xmlNodeMasses.append(n)

    def GenerateNodeForces(self):  # unused
        for i in range(0, len(self.xmlNodes), 1):
            index = ToSolverIndex(i)
            n = self.xmlHolder.GetNodeForce(index)
            self.xmlNodeForces.append(n)

    def GenerateBeamDistrLoads(self):
        for i in range(0, len(self.xmlBeams), 1):
            index = ToSolverIndex(i)
            n = self.xmlHolder.GetBeamDistrLoad(index)
            self.xmlBeamDistrLoads.append(n)

    def GenerateBeamConcLoads(self):
        for i in range(0, len(self.xmlBeams), 1):
            index = ToSolverIndex(i)
            n = self.xmlHolder.GetBeamConcLoad(index)
            self.xmlBeamConcLoads.append(n)

    def GenerateBeamDistrLoadsPerBeam(self, num):
        for i in range(0, len(self.xmlBeams), 1):
            for _ in range(0, num):
                index = ToSolverIndex(i)
                n = self.xmlHolder.GetBeamDistrLoad(index)
                self.xmlBeamDistrLoads.append(n)

    def GenerateBeamConcLoadsPerBeam(self, num):
        for i in range(0, len(self.xmlBeams), 1):
            for _ in range(0, num):
                index = ToSolverIndex(i)
                n = self.xmlHolder.GetBeamConcLoad(index)
                self.xmlBeamConcLoads.append(n)

    def ReplaceConstants(self, buf):
        numStoreys = str(len(self.xmlStoreys))
        numNodes = str(len(self.xmlNodes))
        numBeams = str(len(self.xmlBeams))
        numShells = str(len(self.xmlShells))
        numRestraints = str(len(self.xmlRestraints))
        numBeamDistrLoads = str(len(self.xmlBeamDistrLoads))
        numBeamConcLoads = str(len(self.xmlBeamConcLoads))
        numNodeMasses = str(len(self.xmlNodeMasses))  # zero
        numNodeForces = str(len(self.xmlNodeForces))  # zero
        buf = buf.replace(
            '#NUM_STOREYS', numStoreys).replace(
            '#NUM_NODES', numNodes).replace(
            '#NUM_BEAMS', numBeams).replace(
            '#NUM_SHELLS', numShells).replace(
            '#NUM_RESTRAINTS', numRestraints).replace(
            '#NUM_BEAM_DISTR_LOADS', numBeamDistrLoads).replace(
            '#NUM_BEAM_CONC_LOADS', numBeamConcLoads).replace(
            '#NUM_NODE_MASSES', numNodeMasses).replace(
            '#NUM_NODE_FORCES', numNodeForces)
        buf = buf.replace('#FNAME', self.xmlHolder.GetFName())
        buf = buf.replace(
            '#NUM_LOAD_CASES', '1').replace(
            '#LOAD_CASES', self.xmlHolder.GetLoadCase())
        buf = buf.replace(
            '#NUM_MATERIALS', '1').replace(
            '#MATERIALS', self.xmlHolder.GetMaterial())
        buf = buf.replace(
            '#NUM_SECTIONS', '1').replace(
            '#SECTIONS', self.xmlHolder.GetSection())
        buf = buf.replace(
            '#NUM_SHELL_SECTIONS', '1').replace(
            '#SHELL_SECTIONS', self.xmlHolder.GetShellSection())
        return buf

    def ReplaceStoreys(self, buf):
        s = ''.join(self.xmlStoreys)
        return buf.replace('#STOREYS', s)

    def ReplaceNodes(self, buf):
        s = ''.join(self.xmlNodes)
        return buf.replace('#NODES', s)

    def ReplaceBeams(self, buf):
        s = ''.join(self.xmlBeams)
        return buf.replace('#BEAMS', s)

    def ReplaceShells(self, buf):
        s = ''.join(self.xmlShells)
        return buf.replace('#SHELLS', s)

    def ReplaceRestraints(self, buf):
        s = ''.join(self.xmlRestraints)
        return buf.replace('#RESTRAINTS', s)

    def ReplaceNodeMasses(self, buf):
        s = ''.join(self.xmlNodeMasses)
        return buf.replace('#NODE_MASSES', s)

    def ReplaceNodeForces(self, buf):
        s = ''.join(self.xmlNodeForces)
        return buf.replace('#NODE_FORCES', s)

    def ReplaceBeamDistrLoads(self, buf):
        s = ''.join(self.xmlBeamDistrLoads)
        return buf.replace('#BEAM_DISTR_LOADS', s)

    def ReplaceBeamConcLoads(self, buf):
        s = ''.join(self.xmlBeamConcLoads)
        return buf.replace('#BEAM_CONC_LOADS', s)

    def Generate(self):
        self.GenerateStoreys()
        self.GenerateNodes()
        self.GenerateBeams()
        # self.GenerateShells()
        self.GenerateRestraints()
        # self.GenerateMinimumRestraints()
        # self.GenerateNodeMasses()
        # self.GenerateNodeForces()

        # self.GenerateBeamDistrLoads()
        # self.GenerateBeamConcLoads()
        self.GenerateBeamDistrLoadsPerBeam(10)
        self.GenerateBeamConcLoadsPerBeam(10)

    def Replace(self):
        s = ''
        with open(self.templateFilename, 'rt') as f:
            s = f.read()
        s = self.ReplaceConstants(s)
        s = self.ReplaceStoreys(s)
        s = self.ReplaceNodes(s)
        s = self.ReplaceBeams(s)
        s = self.ReplaceShells(s)
        s = self.ReplaceRestraints(s)
        s = self.ReplaceNodeMasses(s)
        s = self.ReplaceNodeForces(s)
        s = self.ReplaceBeamDistrLoads(s)
        s = self.ReplaceBeamConcLoads(s)
        with open(self.outFilename, 'wt') as f:
            f.write(s)

    def VizualizeFile(self, filename, initialDepth=-10):
        xmlTree = SolverSchemaApi.parse(filename, True)
        renderer = XMLRenderer(xmlTree, initialDepth)
        renderer.Run()

    def Run(self):
        self.Generate()
        self.Replace()
        initialDepth = -max(self.sizeX, self.sizeY, self.sizeZ)
        # self.VizualizeFile(self.outFilename, initialDepth)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--sizeX', default=1, type=int)
    parser.add_argument('--sizeY', default=1, type=int)
    parser.add_argument('--sizeZ', default=1, type=int)
    parser.add_argument('--stepX', default=1, type=int)
    parser.add_argument('--stepY', default=1, type=int)
    parser.add_argument('--stepZ', default=1, type=int)
    parser.add_argument('-o', '--out', default='out.xml', type=str)
    parser.add_argument('--template', default='template.xml', type=str)
    parser.add_argument('--vizFilename', default='', type=str)
    args = parser.parse_args()
    assert args.sizeX > 0 and args.sizeY > 0 and args.sizeZ > 0
    assert args.stepX > 0 and args.stepY > 0 and args.stepZ > 0
    app = App(args.sizeX, args.sizeY, args.sizeZ,
              args.stepX, args.stepY, args.stepZ,
              args.out, args.template)
    if args.vizFilename != '':
        app.VizualizeFile(args.vizFilename)
    else:
        app.Run()
