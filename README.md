XMLHolder
    Utility class, stores XML representations
    Has replacement Getters for fetching XML text

App
    Generators algorithmically construct model, use XMLHolder
    Replacers replace text from template.xml
    Uses solver_schema to construct xml tree in memory, from output xml
    Renderer renders xml tree on screen using pygame window

To Add new Type
    * Define global TypeXML() in XMLHolder.py
    * Install TypeXML() in XMLHolder constructor
    * Define Getter GetTypeXML() in XMLHolder
    * Define optional Generator in App
    * Define Replacer, or extend ReplaceConstants, in App
    * Edit template.xml

pip install generateDS
python generateDS.py -o solver_schema_api.py schema.xsd
pip install pygame
pip install pyOpenGL
